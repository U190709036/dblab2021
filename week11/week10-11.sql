call selectAllCustomers;

call getCustomersByCountry("Spain");

set @country = "UK";
select @country;
call getCustomersByCountry(@country);
select @country;

select * from Shippers;

select count(OrderID)
from Orders join Shippers on Orders.ShipperID = Shippers.ShipperID
where ShipperName = "Speedy Express";

set @orderCount = 0;
call getNumberOfOrdersByShipper("Speedy Express", @orderCount);
select @orderCount;

set @beg = 100;
set @inc = 10;

call counter(@beg, @inc);
select @beg;
select @inc;

CREATE DEFINER=`ceng3005`@`localhost` PROCEDURE `selectAllCustomers`()
BEGIN
	select * from Customers;
END

CREATE DEFINER=`ceng3005`@`localhost` PROCEDURE `getCustomersByCountry`(IN c varchar(45))
BEGIN
	select *
    from Customers
    where Country=c;
    
    set c = "Turkey";
END

CREATE DEFINER=`ceng3005`@`localhost` PROCEDURE `getNumberOfOrdersByShipper`(IN shipper varchar(45), OUT COUNT int)
BEGIN
	select count(OrderID) into count
    from Orders join Shippers on Orders.ShipperID = Shippers.ShipperID
    where ShipperName = shipper;
END

CREATE DEFINER=`ceng3005`@`localhost` PROCEDURE `counter`(INOUT b int, IN inc int)
BEGIN
	set b = b + inc;
    set inc = 50;
END

#########

load data 
infile 'C:\Users\Gizem Özgen\Desktop\MySQL Server 8.0\Uploads\denormalized.csv'
#infile 'C:\Users\Gizem Özgen\Downloads\denormalized.csv'
into table denormalized
columns terminated by ";";

#show variables like "secure_file_priv";

insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinctrow  movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select * from movies;

insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
union
select distinctrow star_country_id, star_country_name
from denormalized
order by producer_country_id;

select * from countries;
